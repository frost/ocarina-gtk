#ifndef SF_MEM_H
#define SF_MEM_H

int load_soundfont_from_memory(fluid_synth_t *synth, fluid_settings_t *settings, const void *start, int size);

#endif
