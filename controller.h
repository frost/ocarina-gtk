#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "definitions.h"

void notePressed(NoteKey note);
void noteReleased(NoteKey note);
void pitchBendPressed(NoteKey key);
void pitchBendReleased(NoteKey key);
void pitchBendAxis(float amount);

#endif
