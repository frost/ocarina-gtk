#ifndef SYNTHESIZER_H
#define SYNTHESIZER_H

#include "definitions.h"

void synth_setup(char *soundfont_file);
void synth_teardown(void);

void synth_play(int key);
void synth_stop(int key);

void playOcarina(NoteKey button);
void stopOcarina(NoteKey button);

void pitchBend(float amount);

#endif
