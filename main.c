#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "synthesizer.h"
#include "gui.h"
#include "sdl.h"

int main (int argc, char **argv) {
	char *soundfont_file = NULL; // use built-in file
	
	if (argc > 1 && strcmp(argv[1], "--help") == 0) {
		printf("Usage: ocarina\n");
		printf("       ocarina [--version]\n");
		return 0;
	}
	if (argc > 1 && strcmp(argv[1], "--version") == 0) {
		printf("Ocarina version %s [branch %s, commit %s]\n", APP_VERSION, GIT_BRANCH, GIT_COMMIT);
		return 0;
	}
	if (argc > 2 && strcmp(argv[1], "--soundfont") == 0) {
		soundfont_file = argv[2];
	}
	
	synth_setup(soundfont_file);
	
#ifdef SDL
	SDLsetup();
#endif
	
	int status = runGUI(argc, argv);
	
#ifdef SDL
	SDLteardown();
#endif
	synth_teardown();
	return status;
}
