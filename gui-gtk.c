#include <gtk/gtk.h>
#include <signal.h>
#include "synthesizer.h"
#include "controller.h"
#include "definitions.h"

#include "compiled-resources/resources.h"

GtkApplication *app;
GtkWidget *window;

GtkWidget *lButton;
GtkWidget *rButton;
GtkWidget *yButton;
GtkWidget *xButton;
GtkWidget *aButton;

/*
 * Key bindings for the keyboard controls.
 *
 * ╭W╮    U       O
 * ╰S╯        I
 *          J   L
 *   E
 *  ─┼─
 *   D
 */
struct {
	int l;
	int r;
	int y;
	int x;
	int a;
	int pitchBendUp;
	int pitchBendDown;
	int shortPitchBendUp;
	int shortPitchBendDown;
} keymap = {
	.l = 'u',
	.r = 'o',
	.y = 'j',
	.x = 'i',
	.a = 'l',
	.pitchBendUp = 'w',
	.pitchBendDown = 's',
	.shortPitchBendUp = 'e',
	.shortPitchBendDown = 'd'
};

/**
 * A GTK button was pressed with the mouse, see what note it was.
 */
static void buttonPressed(GtkWidget *button, gpointer userData) {
	if (button == lButton) {
		notePressed(NK_L);
	} else if (button == rButton) {
		notePressed(NK_R);
	} else if (button == yButton) {
		notePressed(NK_Y);
	} else if (button == xButton) {
		notePressed(NK_X);
	} else if (button == aButton) {
		notePressed(NK_A);
	}
}
/**
 * A GTK button was released with the mouse, see what note it was.
 */
static void buttonReleased(GtkWidget *button, gpointer userData) {
	if (button == lButton) {
		noteReleased(NK_L);
	} else if (button == rButton) {
		noteReleased(NK_R);
	} else if (button == yButton) {
		noteReleased(NK_Y);
	} else if (button == xButton) {
		noteReleased(NK_X);
	} else if (button == aButton) {
		noteReleased(NK_A);
	}
}

/**
 * A key was pressed, see what key it was [Unicode is kludgy]
 * and play that note.
 */
static void keyPressed(GtkWidget *widget, GdkEventKey *event, gpointer userData) {
	int key = gdk_keyval_to_unicode(event->keyval);
	if (key == keymap.l) {
		notePressed(NK_L);
	} else if (key == keymap.r) {
		notePressed(NK_R);
	} else if (key == keymap.y) {
		notePressed(NK_Y);
	} else if (key == keymap.x) {
		notePressed(NK_X);
	} else if (key == keymap.a) {
		notePressed(NK_A);
	} else if (key == keymap.pitchBendUp) {
		pitchBendPressed(NK_PitchBendUp);
	} else if (key == keymap.pitchBendDown) {
		pitchBendPressed(NK_PitchBendDown);
	} else if (key == keymap.shortPitchBendUp) {
		pitchBendPressed(NK_ShortPitchBendUp);
	} else if (key == keymap.shortPitchBendDown) {
		pitchBendPressed(NK_ShortPitchBendDown);
	}
}
/**
 * A key was released, see what key it was [Unicode is kludgy]
 * and release that note.
 */
static void keyReleased(GtkWidget *widget, GdkEventKey *event, gpointer userData) {
	int key = gdk_keyval_to_unicode(event->keyval);
	if (key == keymap.l) {
		noteReleased(NK_L);
	} else if (key == keymap.r) {
		noteReleased(NK_R);
	} else if (key == keymap.y) {
		noteReleased(NK_Y);
	} else if (key == keymap.x) {
		noteReleased(NK_X);
	} else if (key == keymap.a) {
		noteReleased(NK_A);
	} else if (key == keymap.pitchBendUp) {
		pitchBendReleased(NK_PitchBendUp);
	} else if (key == keymap.pitchBendDown) {
		pitchBendReleased(NK_PitchBendDown);
	} else if (key == keymap.shortPitchBendUp) {
		pitchBendReleased(NK_ShortPitchBendUp);
	} else if (key == keymap.shortPitchBendDown) {
		pitchBendReleased(NK_ShortPitchBendDown);
	}
}

/**
 * Start and run the GTK+ GUI.
 * This function only returns when the GUI exits!
 */
int runGUI(int argc, char **argv) {
	gtk_init(&argc, &argv);
	
	// Load the UI from file
	GtkBuilder *builder = gtk_builder_new_from_resource("/ocarina/ui/gtk-ui.glade");
	
	window = GTK_WIDGET(gtk_builder_get_object(builder, "mainWindow"));
	
	// quit when the window's closed
	g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
	
	// set the buttons
	lButton = GTK_WIDGET(gtk_builder_get_object(builder, "lButton"));
	rButton = GTK_WIDGET(gtk_builder_get_object(builder, "rButton"));
	yButton = GTK_WIDGET(gtk_builder_get_object(builder, "yButton"));
	xButton = GTK_WIDGET(gtk_builder_get_object(builder, "xButton"));
	aButton = GTK_WIDGET(gtk_builder_get_object(builder, "aButton"));
	
	// wire up callbacks
	gtk_builder_add_callback_symbol(builder, "buttonPressed", G_CALLBACK(buttonPressed));
	gtk_builder_add_callback_symbol(builder, "buttonReleased", G_CALLBACK(buttonReleased));
	
	// set up keyboard events from the window
	gtk_widget_add_events(window, GDK_KEY_PRESS_MASK | GDK_KEY_RELEASE_MASK);
	g_signal_connect(window, "key-press-event", G_CALLBACK(keyPressed), NULL);
	g_signal_connect(window, "key-release-event", G_CALLBACK(keyReleased), NULL);
	
	// actually connect the callbacks from the file
	gtk_builder_connect_signals(builder, NULL);
	
	// handle ^C normally
	signal(SIGINT, SIG_DFL);
	
	// go!
	gtk_main();
	return 0;
}
