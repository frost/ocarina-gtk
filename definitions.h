#ifndef DEFINITIONS_H
#define DEFINITIONS_H

typedef char bool;
#define false 0
#define true  1
#define off 0
#define on  1

typedef enum {
	NK_NONE = 0,
	NK_L,
	NK_R,
	NK_Y,
	NK_X,
	NK_A,
	NK_PitchBendUp,
	NK_PitchBendDown,
	NK_ShortPitchBendUp,
	NK_ShortPitchBendDown
} NoteKey;

#endif
