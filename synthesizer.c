#include <fluidsynth.h>
#include <assert.h>
#include "definitions.h"
#include "soundfont-from-memory.h"

// for GResource, to load the soundfont
#include <gio/gio.h>
#include "compiled-resources/resources.h"

fluid_synth_t *synth = NULL;
fluid_settings_t *settings = NULL;
fluid_audio_driver_t *driver = NULL;

/*
 * The ocarina tuning.
 * Found by experiment.
 */
struct {
	int l;
	int r;
	int y;
	int x;
	int a;
} ocarina_pitches = {
	.l = 74,
	.r = 77,
	.y = 81,
	.x = 83,
	.a = 86
};

void synth_setup(char *soundfont_location) {
	assert(synth == NULL);
	
	settings = new_fluid_settings();
	// ocarinas can't play more than one note at a time
	fluid_settings_setint(settings, "synth.polyphony", 1);
	
	synth = new_fluid_synth(settings);
	
	// don't use JACK
	fluid_settings_setstr(settings, "audio.driver", "pulseaudio");
	driver = new_fluid_audio_driver(settings, synth);
	
	if (soundfont_location != NULL) {
		// load the soundfont
		printf("Loading soundfont from %s\n", soundfont_location);
		if (fluid_synth_sfload(synth, soundfont_location, 1) == FLUID_FAILED) {
			printf("Can't load soundfont!\n");
			exit(1);
		}
	} else {
		// load the soundfont from memory!
		GError *err = NULL;
		GResource *resource = gresources_get_resource();
		GBytes *soundfont_resource = g_resource_lookup_data(resource, "/ocarina/soundfont/MuseScore_General_Ocarina.sf2", 0, &err);
		if (err != NULL) {
			fprintf(stderr, "Can't load soundfont from the binary: %s\n", err->message);
			exit(1);
		}
		
		gsize size;
		const void *soundfontPtr = g_bytes_get_data(soundfont_resource, &size);
		load_soundfont_from_memory(synth, settings, soundfontPtr, size);
	}
	
	// Select the ocarina!
	// last two are bank and preset
	fluid_synth_program_select(synth, 0, 1, 0, 79);
	
	// Make it a bit louder
	fluid_settings_setnum(settings, "synth.gain", 0.8);
	
	// Change the pitch wheel sensitivity so it can go to -3..3 like the game can (if you use both stick and D-pad)
	fluid_synth_pitch_wheel_sens(synth, 0, 3);
}

void synth_teardown(void) {
	assert(synth != NULL);
	fluid_synth_sfunload(synth, 1, 0);
	delete_fluid_audio_driver(driver);
	delete_fluid_synth(synth);
	delete_fluid_settings(settings);
}

void synth_play(int key) {
	fluid_synth_noteon(synth, 0, key, 100);
}
void synth_stop(int key) {
	fluid_synth_noteoff(synth, 0, key);
}

////////// Ocarina stuff //////////

/*
 * What notes are currently held down?
 */
static struct {
	bool l;
	bool r;
	bool y;
	bool x;
	bool a;
} ocarinastate = {off, off, off, off, off};

/*
 * Turn everything off.
 */
static void resetOcarinaStateArray(void) {
	ocarinastate.l = off;
	ocarinastate.r = off;
	ocarinastate.y = off;
	ocarinastate.x = off;
	ocarinastate.a = off;
}

/*
 * Play an ocarina note!
 * Checks the ocarinastate array to make sure the note isn't already playing,
 * because if it is you'll get a nasty little click.
 * The ocarinastate array acts kinda like radio buttons,
 * turn the rest off when playing a note.
 */
void playOcarina(NoteKey button) {
	switch (button) {
		case NK_L:
			if (!ocarinastate.l) {
				resetOcarinaStateArray();
				ocarinastate.l = on;
				synth_play(ocarina_pitches.l);
			}
			break;
		case NK_R:
			if (!ocarinastate.r) {
				resetOcarinaStateArray();
				ocarinastate.r = on;
				synth_play(ocarina_pitches.r);
			}
			break;
		case NK_Y:
			if (!ocarinastate.y) {
				resetOcarinaStateArray();
				ocarinastate.y = on;
				synth_play(ocarina_pitches.y);
			}
			break;
		case NK_X:
			if (!ocarinastate.x) {
				resetOcarinaStateArray();
				ocarinastate.x = on;
				synth_play(ocarina_pitches.x);
			}
			break;
		case NK_A:
			if (!ocarinastate.a) {
				resetOcarinaStateArray();
				ocarinastate.a = on;
				synth_play(ocarina_pitches.a);
			}
			break;
		default:
			break;
	}
	// printf("[synth] ocarinastate: %d %d %d %d %d\n", ocarinastate.l, ocarinastate.r, ocarinastate.y, ocarinastate.x, ocarinastate.a);
}
/**
 * Stop an ocarina note.
 * Turn off the ocarinastate for that note if it's not already.
 */
void stopOcarina(NoteKey button) {
	switch (button) {
		case NK_L:
			ocarinastate.l = off;
			synth_stop(ocarina_pitches.l);
			break;
		case NK_R:
			ocarinastate.r = off;
			synth_stop(ocarina_pitches.r);
			break;
		case NK_Y:
			ocarinastate.y = off;
			synth_stop(ocarina_pitches.y);
			break;
		case NK_X:
			ocarinastate.x = off;
			synth_stop(ocarina_pitches.x);
			break;
		case NK_A:
			ocarinastate.a = off;
			synth_stop(ocarina_pitches.a);
			break;
		default:
			break;
	}
}

/**
 * Do pitch bend, converting it from -3..3 to -16384..16383.
 */
void pitchBend (float amount) {
	int converted = (amount * 8192/3.0) + 8192;
	if (converted < 0) {
		converted = 0;
	}
	if (converted > 16383) {
		converted = 16383;
	}
	// printf("Pitch bend! %g => %d\n", amount, converted);
	fluid_synth_pitch_bend(synth, 0, converted);
}
