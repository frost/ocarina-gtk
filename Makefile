#
# Makefile for ocarina
# Created by Timberwolf on 2020-11-10
#

APP_VERSION=0.0.1

CFLAGS+=-g -Wall
LFLAGS=-lfluidsynth `pkg-config gtk+-3.0 --cflags --libs`
SDL_FLAGS=`pkg-config sdl2 --cflags --libs 2> /dev/null`
HAS_SDL != pkg-config sdl2 --cflags --libs > /dev/null 2>&1 && echo "-DSDL -lpthread" || echo

GRESOURCE_DIR=compiled-resources

GIT_COMMIT=`git rev-parse HEAD`
GIT_BRANCH=`git branch --show-current`
VERSION_FLAGS=-DAPP_VERSION="\"${APP_VERSION}\"" -DGIT_COMMIT="\"${GIT_COMMIT}\"" -DGIT_BRANCH="\"${GIT_BRANCH}\""

ocarina: *.c resources
	${CC} ${CFLAGS} -o ocarina *.c ${GRESOURCE_DIR}/*.c ${LFLAGS} ${SDL_FLAGS} ${HAS_SDL} ${VERSION_FLAGS}

resources: gtk-ui.glade
	mkdir -p ${GRESOURCE_DIR}
	glib-compile-resources --internal gresources.xml --generate-source --target=${GRESOURCE_DIR}/resources.c
	glib-compile-resources --internal gresources.xml --generate --target=${GRESOURCE_DIR}/resources.h

clean:
	rm -f ocarina
	rm -f *.o
	rm -f ${GRESOURCE_DIR}/resources.c
	rm -f ${GRESOURCE_DIR}/resources.h
	rmdir ${GRESOURCE_DIR}
