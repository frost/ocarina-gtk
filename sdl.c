#ifdef SDL

#include <SDL2/SDL.h>

#include <pthread.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include "definitions.h"
#include "controller.h"

// static int controllerEvent(void *userData, SDL_Event *event);
static void *mainLoop(void *ignored);

SDL_GameController *controller1 = NULL;

pthread_t controllerLoopThread;

void SDLsetup(void) {
	printf("Initializing SDL for controller support\n");
	
	if (SDL_Init(SDL_INIT_GAMECONTROLLER | SDL_INIT_EVENTS) != 0) {
		fprintf(stderr, "Unable to start SDL: %s\n", SDL_GetError());
	}
	
	printf("%d controller%s available\n", SDL_NumJoysticks(), (SDL_NumJoysticks()==1 ? "" : "s"));
	// grab the first available controller
	for (int i = 0; i < SDL_NumJoysticks(); i++) {
		controller1 = SDL_GameControllerOpen(i);
		if (controller1) {
			break; // got one!
		}
	}
	if (controller1 == NULL) {
		// no controllers.
		return;
	}
	
	printf("Starting SDL main loop\n");
	if (pthread_create(&controllerLoopThread, NULL, mainLoop, NULL) != 0) {
		fprintf(stderr, "Unable to create controller main loop: %s\n", strerror(errno));
	}
}

void SDLteardown(void) {
	if (controller1) {
		printf("Closing controller 1\n");
		SDL_GameControllerClose(controller1);
	}
	if (controllerLoopThread) {
		printf("Stopping controller thread.\n");
		pthread_cancel(controllerLoopThread);
	}
	SDL_Quit();
}

/**
 * Some controller button got pressed.
 */
static void buttonStateChanged(SDL_GameControllerButton button, int state) {
	// printf("Button %d %s\n", button, (state == SDL_PRESSED) ? "pressed" : "released");
	
	NoteKey note      = NK_NONE;
	NoteKey pitchBend = NK_NONE;
	switch (button) {
		case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
			note = NK_L;
			break;
		case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
			note = NK_R;
			break;
		case SDL_CONTROLLER_BUTTON_X:
			note = NK_Y;
			break;
		case SDL_CONTROLLER_BUTTON_Y:
			note = NK_X;
			break;
		case SDL_CONTROLLER_BUTTON_B:
			note = NK_A;
			break;
		case SDL_CONTROLLER_BUTTON_DPAD_UP:
			pitchBend = NK_ShortPitchBendUp;
			break;
		case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
			pitchBend = NK_ShortPitchBendDown;
			break;
		default:
			break;
	}
	if (note) {
		if (state == SDL_PRESSED) {
			notePressed(note);
		} else {
			noteReleased(note);
		}
	}
	if (pitchBend) {
		if (state == SDL_PRESSED) {
			pitchBendPressed(pitchBend);
		} else {
			pitchBendReleased(pitchBend);
		}
	}
}

/**
 * Main controller loop.
 * Continuously look for controller input.
 * Make sure to run it on its own thread, as it never returns!
 * When you're done with it, just pthread_cancel() the thread.
 */
static void *mainLoop(void *ignored) {
	while (1) {
		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			// printf("Got SDL event\n");
			switch (event.type) {
				case SDL_CONTROLLERBUTTONDOWN:
				case SDL_CONTROLLERBUTTONUP:
					buttonStateChanged(event.cbutton.button, event.cbutton.state);
					break;
				case SDL_CONTROLLERAXISMOTION:
					if (event.caxis.axis == SDL_CONTROLLER_AXIS_LEFTY) {
						float axis = event.caxis.value;
						
						// make it match the negative side
						if (axis >= 32767) {
							axis = 32768;
						}
						axis *= -1; // flip so positive is up
						axis /= 32768/2; // scale to -2..2
						
						// deadzone - make close-to-zero actually zero
						if (fabsf(axis) < 0.2) {
							axis = 0.0;
						}
						
						pitchBendAxis(axis);
					}
					break;
				default:
					// printf("Unused controller event\n");
					break;
			}
		}
		sleep(0); // let pthreads cancel here
	}
	return NULL;
}

#endif
