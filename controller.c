#include <stdio.h>
#include "synthesizer.h"
#include "definitions.h"

/**
 * Keep track of which notes are currently held down and in what order.
 */
static const int nNoteKeys = 5;
static const int nPitchBendKeys = 4;
static NoteKey currentlyPlayingStack[5] = {
	NK_NONE,
	NK_NONE,
	NK_NONE,
	NK_NONE,
	NK_NONE
};
static NoteKey pitchBendStack[4] = {
	NK_NONE,
	NK_NONE,
	NK_NONE,
	NK_NONE
};

static float pitchBendAxisAmount = 0.0;

////////// functions //////////

/**
 * Debug-print the contents of a note stack.
 * n is the size of the stack.
 */
static void debugPrintNoteStack(char *desc, NoteKey stack[], int n) {
	printf("%s:", desc);
	for (int i = 0; i < n; i++) {
		printf(" %d", stack[i]);
	}
	printf("\n");
}
/**
 * Append a note key to the specified stack, if there's room.
 * n is the size of the stack.
 * Returns true if the stack was modified, false if it wasn't.
 */
static bool pushCurrentNote(NoteKey stack[], int n, NoteKey note) {
	for (int i = 0; i < n; i++) {
		if (stack[i] == note) {
			// Oops, it's already on here!
			// This can happen with key repeat.
			return false;
		}
		if (stack[i] == NK_NONE) {
			stack[i] = note;
			return true;
		}
	}
	return false;
}
/**
 * Remove a note from the specified stack, wherever it is.
 * n is the size of the stack.
 * Returns true if the stack was modified, false if it wasn't.
 */
static bool rmCurrentNote(NoteKey stack[], int n, NoteKey note) {
	bool needsShifting = off;
	int i;
	for (i = 0; i < n; i++) {
		if (stack[i] == note) {
			stack[i] = NK_NONE;
			needsShifting = on;
			break;
		}
	}
	if (needsShifting) {
		// shift everything down to close the gap
		for (; i < n-1; i++) {
			stack[i] = stack[i+1];
		}
		stack[i] = NK_NONE;
		return true;
	} else {
		return false;
	}
}

/**
 * Keep any other notes that were held down still playing.
 * The synth will take care of avoiding duplicates [which make clicks].
 * If a note is released, fall back to the most recently played note that's still held down
 * [which is what rmCurrentNote() is for].
 */
static void recomputeNoteState() {
	NoteKey noteToPlay = NK_NONE;
	for (int i = 0; i < nNoteKeys; i++) {
		NoteKey note = currentlyPlayingStack[i];
		if (note == NK_NONE) {
			// the last one was the right one
			// [or there was no last one
			//  in which case, play nothing]
			break;
		} else {
			noteToPlay = note;
		}
	}
	if (noteToPlay) {
		playOcarina(noteToPlay);
	} else {
		stopOcarina(NK_L);
		stopOcarina(NK_R);
		stopOcarina(NK_Y);
		stopOcarina(NK_X);
		stopOcarina(NK_A);
	}
}
/**
 * Figure out what the current pitch bend is and play it.
 */
static void recomputePitchBend() {
	float pitchBendAdjust = 0;
	
	// add axis to any discrete keys
	if (pitchBendAxisAmount != 0) {
		pitchBendAdjust = pitchBendAxisAmount;
	}
	
	for (int i = 0; i < nPitchBendKeys; i++) {
		NoteKey key = pitchBendStack[i];
		if (key == NK_NONE) {
			// the last one was the right one
			// [or there was no last one
			//  in which case, play nothing]
			continue;
		}
		// play the actual pitch bend
		switch (key) {
			case NK_PitchBendUp:
				pitchBendAdjust += 2.0;
				break;
			case NK_PitchBendDown:
				pitchBendAdjust += -2.0;
				break;
			case NK_ShortPitchBendUp:
				pitchBendAdjust += 1.0;
				break;
			case NK_ShortPitchBendDown:
				pitchBendAdjust += -1.0;
				break;
			default:
				break;
		}
	}
	printf("pitchbend: %g\n", pitchBendAdjust);
	pitchBend(pitchBendAdjust);
}

void notePressed(NoteKey note) {
	// add it to the note stack
	bool added = pushCurrentNote(currentlyPlayingStack, nNoteKeys, note);
	if (added == true) {
		recomputeNoteState();
		debugPrintNoteStack("[add      note]", currentlyPlayingStack, nNoteKeys);
	}
}
void noteReleased(NoteKey note) {
	// remove it from the note stack
	bool removed = rmCurrentNote(currentlyPlayingStack, nNoteKeys, note);
	if (removed == true) {
		recomputeNoteState();
		debugPrintNoteStack("[rm       note]", currentlyPlayingStack, nNoteKeys);
	}
}
void pitchBendPressed(NoteKey key) {
	// add it to the pitch bend stack
	bool added = pushCurrentNote(pitchBendStack, nPitchBendKeys, key);
	if (added) {
		recomputePitchBend();
		debugPrintNoteStack("[add pitchbend]", pitchBendStack, nPitchBendKeys);
	}
}
void pitchBendReleased(NoteKey key) {
	// remove it from the pitch bend stack
	rmCurrentNote(pitchBendStack, nPitchBendKeys, key);
	recomputePitchBend();
	debugPrintNoteStack("[rm  pitchbend]", pitchBendStack, nPitchBendKeys);
}

void pitchBendAxis(float amount) {
	if (amount == pitchBendAxisAmount) {
		// it wasn't changed from last time
		return;
	}
	printf("[axis %f]\n", amount);
	pitchBendAxisAmount = amount;
	recomputePitchBend();
}
