# Ocarina

This is a prototype clone of the ocarina from The Legend Of Zelda: Ocarina of Time.

## Dependencies

#### Runtime dependencies

- GTK3 [you probably already have this installed]
- libfluidsynth2
- libsdl2-2.0-0

#### Build dependencies

- libgtk3-dev
- libfluidsynth-dev
- libsdl2-dev if you want controller support

## Compiling

```
make resources
make
```

If you need to get rid of the build files, you can use `make clean`.

You'll only need to run `make resources` once, unless you do a clean or change the soundfont or UI.

## Controls

Controllers are supported with SDL!
X/Y and A/B are swapped to match the Nintendo layout.

#### Keyboard controls

Currently it's set up with the following keys:

```
L: U
R: O
Y: J
X: I
A: L
stick up/down: W/S
D-pad up/down: E/D

╭W╮    U       O
╰S╯        I
         J   L
  E
 ─┼─
  D
```

This is currently not configurable, but hopefully will be in the future. The buttons also work, of course, but that's clunky.
