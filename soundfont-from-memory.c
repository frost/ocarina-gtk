#include <stdio.h>
#include <stdlib.h>
#include <fluidsynth.h>
#include <stdint.h>

// see http://www.fluidsynth.org/api/fluidsynth_sfload_mem_8c-example.html

unsigned long long currentpos = 0;
unsigned long long size;

void *sfmem_open(const char *filename) {
	printf("Loading soundfont...\n");
	void *p;
	if(filename[0] != '&') {
		return NULL;
	}
	sscanf(filename, "&%p", &p);
	return p;
}
int sfmem_read(void *buf, int count, void *handle) {
	if (currentpos + count >= size) {
		// out of bounds!
		return FLUID_FAILED;
	}
	for (int i = 0; i < count; i++) {
		// copy bytes
		((uint8_t *)buf)[i] = ((uint8_t *)handle)[currentpos];
		currentpos++;
	}
	return FLUID_OK;
}
int sfmem_seek(void *handle, long offset, int origin) {
	switch (origin) {
		case SEEK_SET:
			currentpos = offset;
			break;
		case SEEK_CUR:
			currentpos += offset;
			break;
		case SEEK_END:
			currentpos = size - offset;
			break;
	}
	return FLUID_OK;
}
int sfmem_close(void *handle) {
	printf("done!\n");
	return FLUID_OK;
}
long sfmem_tell(void *handle) {
	return currentpos;
}

int load_soundfont_from_memory(fluid_synth_t *synth, fluid_settings_t *settings, const void *start, int filesize) {
	size = filesize;
	
	fluid_sfloader_t *sfmem_sfloader = new_fluid_defsfloader(settings);
	fluid_sfloader_set_callbacks(sfmem_sfloader,
	                             sfmem_open,
	                             sfmem_read,
	                             sfmem_seek,
	                             sfmem_tell,
	                             sfmem_close);
	fluid_synth_add_sfloader(synth, sfmem_sfloader);
	
	char abused_filename[64];
	sprintf(abused_filename, "&%p", start);
	int id = fluid_synth_sfload(synth, abused_filename, 0);
	/* now sfmem_open() will be called with abused_filename and should have opened the memory region */
	if (id == FLUID_FAILED) {
		fprintf(stderr, "Can't load soundfont from memory!\n");
		exit(1);
	}
	
	return 0;
}
